tap "homebrew/bundle"       # First line of a bundle
tap "homebrew/cask"         # Not needed on command line
tap "homebrew/cask-fonts"   # Just needed for font casks below
tap "homebrew/core"         # Not needed on command line

# Building tools
brew "boost"        # C++ library
brew "ccache"       # Faster builds by caching
brew "cmake"        # Build software projects
brew "ninja"        # Replacement for make
brew "doxygen"      # Doxygen generates C++ documentation
brew "pre-commit"   # Allows pre-commit hooks to be installed and managed
brew "tbb"          # Threaded building blocks from Intel
brew "swig"         # Software wrapper interface generator
brew "qt"           # The Qt Toolkit

# General utilities
brew "colordiff"    # More colorful diffs outside of git
brew "coreutils"    # Basic stuff with a g prefix
brew "gnu-sed"      # Adds the gsed command, more powerful than BSD sed
brew "gnu-time"     # Nicer timing
brew "openssl"      # Security stuff
brew "git"          # The latest version of git instead of Apple's older one
brew "git-gui"      # A quicker way to apply partial changes
brew "htop"         # htop is better than top for checking processes
brew "tree"         # tree is nice for looking at directories
brew "wget"         # Mac's have curl by default, but not wget
brew "bash"         # Bash 5 instead of 3, in case you need it
brew "rename"       # Rename files utility
brew "clang-format" # Format C++ files
brew "tmux"         # Split windows and saving terminal sessions (screen replacement)
brew "gh"           # GitHub's command line interface, from gh's tap
brew "bat"          # Nicely colorized replacement for cat

# Personal customization options
brew "fish"         # My favorite shell. Might move to zsh when macOS does, though
brew "lmod"         # See my posts on lmod
brew "macvim"       # VI for macOS, with mvim graphical interface too
brew "interactive-rebase-tool" # Run git config --global sequence.editor interactive-rebase-tool
brew "bash-completion" # Nicer completion for bash if you use it

# Programming languages
brew "python"       # Python 3.8
brew "numpy"        # Now is Python3 only (numpy@1.16 is for python@2)
brew "go"           # Used by hugo, can be useful to have
brew "node"         # Javascript (for gitbooks, etc)
brew "yarn"         # Package manager for node.js
brew "ruby"         # Just to be extra sure the system Ruby never gets modified
brew "rbenv"        # Use this for Ruby (pyenv also exists)
brew "rust"         # Was trying out mdbook
brew "lua"          # Lightweight language like Python
brew "java"         # Meh. What can I say?

# Python programs
brew "pipx"         # Better way to add PyPI applications
brew "pipenv"       # All-in-one environment tool
brew "nox"          # Tool for standard development environments
brew "tox"          # Old tool for standard development environments
brew "poetry"       # Nice all-in-one packaging tool
brew "jupyterlab"   # Programming environment
brew "black"        # Python formatting
brew "mypy"         # Python type checking
brew "cookiecutter" # Quickly start new projects

# Packages
brew "hugo"         # Fast website generator
brew "pandoc"       # Convert between document formats
brew "pdftk-java"   # PDF Tool Kit (Java port)
brew "qt"           # The #1 graphics library for C++ and Python
brew "root"         # High Energy Physics toolkit

brew "libsodium"    # Have no idea why I needed this


# Fonts
cask "font-hack-nerd-font"
cask "font-sauce-code-pro-nerd-font"

# Core
cask "iterm2"       # A great terminal
cask "mactex"       # LaTeX. Huge.
cask "miniconda"    # Nice way to get a system Conda install
cask "java"         # The programming language vm

# Programs
cask "google-chrome"# Since once and a while a site doesn't work with Safari
cask "gimp"         # Photo editor
cask "blender"      # The 3D application
cask "inkscape"     # 2D vector drawings

# Editors
cask "macdown"      # Nice Markdown
cask "texstudio"    # Nice IDE for LaTeX
cask "meld"         # Compare files graphically.
cask "tikzit"       # Fast drawings
cask "visual-studio-code"

# Daemons
cask "docker"       # Allows running and building docker images
cask "dropbox"      # The cloud
cask "synergy"      # Share a mouse and keyboard between computers. Free option is okay.
cask "amethyst"     # Simulate a non-overlapping window manager with keyboard shortcuts
cask "xquartz"      # Legacy Linux apps may need this

# Chat
cask "mattermost"
cask "skype"
cask "slack"
cask "element"
